---
title: Khadas VIM3 Pro
---

This document provides information needed for adding [Khadas' VIM3 Pro](https://www.khadas.com/vim3) boards to the LAVA lab.

Note that this isn't about the VIM3 Basic board, which has less memory (2GB vs 4GB RAM and 16GB vs. 32GB eMMC).

Specification:
* Amlogic A311D SoC
* 2.2GHz Quad core ARM Cortex-A73 and 1.8GHz dual core Cortex-A53 CPU
* 4GB of RAM
* ARM Mali G52 MP4 GPU 800MHz
* 1 USB 3.0 port (USB-A)
* 1 USB 2.0 port (USB-A)
* 1 USB 2.0 port (USB-C) with power delivery (5-20V input)
* 1 RJ-45 port
* 40-Pin GPIO which includes pins for the SoC UART
* 32GB eMMC

This board is available from [Khadas' store](https://www.khadas.com/product-page/vim3).

### Power control

The VIM3 can be powered via the USB-C port (5-20V input).

It can also be powered via the VIN port on the back of the board, with a
standard 4 circuit, Molex 78172-0004 connector (5-12V input):

![Vim3 bottom interface](/img/vim3_interfaces_bottom.jpg)

### Low-level boot control


- Baud rate: 115200 8n1
- Voltage: 3.3V TTL
- Pins: 17 is GND, 18 is Linux\_RX, 19 is Linux\_TX on the 40 pin extension header

![Serial pinout](/img/vim3_SerialConnections_3Pin.jpg)

#### Network connectivity

The onboard ethernet should work with the vendor U-Boot already flashed.

A random MAC address is generated if there's none defined in the environement. To keep the same MAC address, just do a saveenv in U-Boot.

### Bootloader

The Vim3 boards come with u-boot preflashed which should be updated to mainline
U-Boot.

The bootloader resides in the 16MB SPI-Flash on board (not verified).

Recovering or flashing the bootloader over USB needs to be investigated.

Updating the bootloader from vendor U-Boot can be done as follows:

- download `khadas-vim3.bin` from https://gitlab.com/amlogic-foss/amlogic-u-boot-autotest/-/packages/23045606
- put on an SD card in a fat partition
- boot into vendor U-Boot and run:

```
kvim3#mmc dev 0

kvim3#load mmc 0 1080000 khadas-vim3.bin

kvim3#sf probe
kvim3#sf erase 0 +$filesize
kvim3#sf write 1080000 0 $filesize

kbi bootmode w spi

kbi poweroff
```

### Health checks

The board is supported in the upstream Linux kernel starting from at least
linux 5.8. Board dtb is: `amlogic/meson-g12b-a311d-khadas-vim3.dtb`

### Lab notes and trouble shooting

None known.
