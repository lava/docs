---
title: Dispatcher setup
---

# [Cyberserver Xeon E-RS100-E100](https://www.broadberry.co.uk/xeon-e-rackmount-servers/cyberserve-rs100-e10-pi2) 

## Dispatcher access
To be able to set up and access the lava dispatchers you will need to have your 
SSH keys added to the Collabora infrastructure. To do this you will need to set 
up an ssh key pair that can connect through the [Collabora Bastion server](https://sysadmin.pages.collabora.com/docs/user-guides/bastion-ssh/). 
If you have not set this up yet, please open and Sysadmin Phab request, stating 
that you will need access to the lava dispatchers and you can continue when 
this is complete. 

## Replacement dispatcher

Replacement dispatcher can be installed the same as a new dispatcher up to 
the ansible setup section where the following amendments neeed to be made:

Create a [sysadmin support request](https://phabricator.collabora.com/maniphest/task/edit/form/3/) 
asking for the mac addresses to be updated on the router for the ipmi interface 
and the network interface. This needs to be coordinated with the maintenance 
window to change the dispatchers over as the current one needs to remain until 
then. This will be updated on the router directly for the cambridge office or 
via the [infrastructure repo lava dnsmasq role](https://gitlab.collabora.com/sysadmin/infrastructure/-/blob/main/ansible/roles/role-lava-dnsmasq/files/dnsmasq/dhcp-lava-hosts.conf?ref_type=heads) for the SJIC lab.

We can then continue to set up the server using the [ansible configuration](https://gitlab.collabora.com/lava/collabora-lava-setup/-/tree/master/ansible?ref_type=heads).

### Ansible processing

For this guide we will assume you are setting up a disptcher for 
lava-rack-sjic-12. 

From the collabora-lava-setup/ansible folder double check that the [interface name is correct for the vlan_raw_device variable](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/ansible/hosts/lava-rack-sjic-12/vars/vlan.yml?ref_type=heads). You can then run the ansible to update the dispatcher with:

```
./hosts/lava-rack-sjic-12/deploy/all
```

When this is complete you can check the configuration files on the dispatcher:

```
/etc/google-docker-servo.conf
/etc/pdudaemon/pdudaemon.conf
/etc/conserver/conserver.cf
/etc/udev/rules.d/99-usb-serial.rules
/etc/dnsmasq.d/dhcp.conf
```

## New dispatcher

Dispatcher can be set up as per [these guidelines](https://sysadmin.pages.collabora.com/docs/lava/dispatcher-installation/).

Once you have the mac addresses for the ethernet and the IPMI, open a sysadmin
support request highlighting the need for static IP's on the router in the lava
DMZ. Provide the prefered name of the device along with the identified mac 
addresses for the IPMI interface and the network for the dispatcher. This can be processed at any time before the dispatcher needs to be available.

For this guide we will create a dispatcher called lava-rack-cbg-x

### Setup

For the new dispatcher we will need to create a folder called `lava-rack-cbg-x` 
in the [collabora-lava-setup repo](https://gitlab.collabora.com/lava/collabora-lava-setup/) 
under `ansible->hosts->lava-rack-cbg-x`. from here you will need the folowing 
files and folders creating:

```
.
├── deploy
│   ├── all
│   ├── apt
│   ├── console
│   ├── controller
│   ├── dispatcher
│   ├── dnsmasq
│   ├── iptables
│   └── vlan
└── vars
    ├── controller.yml
    ├── dispatcher.yml
    └── vlan.yml

3 directories, 11 files
```

To run the ansible you can just use the deploy/all deployment script. The other 
files in the deploy folder enable you to run only individual roles. So to only 
update the dnsmasq configuration you can just run the dnsmasq deployment script.

These can be copied from one of the other folders and modified or can be 
created manually with the below commands:

```
mkdir deploy vars
```

```
cat > deploy/all <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/apt.yml \
    deploy/hosts/playbooks/dnsmasq.yml \
    deploy/hosts/playbooks/iptables.yml \
    deploy/hosts/playbooks/controller.yml \
    deploy/hosts/playbooks/dispatcher.yml \
    deploy/hosts/playbooks/vlan.yml \
    deploy/hosts/playbooks/console.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/lava.yml \
    -i inventory/collabora/staging/debian12.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

```
cat > deploy/apt <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/apt.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/lava.yml \
    -i inventory/collabora/staging/debian12.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

```
cat > deploy/console <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/console.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/debian12.yml \
    -i inventory/collabora/staging/lava.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

```
cat > deploy/controller <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/controller.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/lava.yml \
    -i inventory/collabora/staging/debian12.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

```
cat > deploy/dispatcher <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/dispatcher.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/lava.yml \
    -i inventory/collabora/staging/debian12.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

```
cat > deploy/dnsmasq <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/dnsmasq.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/lava.yml \
    -i inventory/collabora/staging/debian12.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

```
cat > deploy/iptables <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/iptables.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/lava.yml \
    -i inventory/collabora/staging/debian12.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

```
cat > deploy/vlan <<EOF
#!/usr/bin/env bash

set -euo pipefail

ANSIBLE_ARGS=$@

cd ${ANSIBLE_LAVA} && \
ansible-playbook \
    deploy/hosts/playbooks/vlan.yml \
    --limit lava-rack-cbg-x \
    -i inventory/collabora/staging/lava.yml \
    -e RUN_FROM_SCRIPT=true \
    -vv ${ANSIBLE_ARGS}
EOF
```

The files in the vars folder will need updating with the relevant details. The 
vlan.yml file will need the interface name of the active network connection 
for the `vlan_raw_device` value:

```
cat > vars/vlan.yml <<EOF
---

vlan_raw_device: enp1s0
vlan_domain: lava-rack
EOF
```

The dispatcher.yml file will need the correct worker token for lava-rack-cbg-x 
from lava either by running `lavacli workers show lava-rack-cbg-x` or from the 
[administration web interface](https://lava.collabora.dev/admin/lava_scheduler_app/worker/):

```
cat vars/dispatcher.yml <<EOF
---

lava_worker_token: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
EOF
```

The main focus is the `deploy/controller.yml` file. This is where the 
configuration variables for all of the devices is held for this dispatcher. As 
an example, we will create one chromebook on a servoMicro, one on a suzyQable 
one SBC and an ethernet enabled PDU:

```
cat vars/controller.yml <<EOF
---

duts:
  chromebooks:
    - name: dell-latitude-3445-7520c-skyrim-cbg-0
      servod: docker
      console: Ti50
      serial: 1982002d-4c1b4b03
      port: 61110
      type: skyrim
      cr50_log: true
      ec_log: true
      mac: 00:13:3b:00:0f:9f
      ip: 192.168.201.10
    - name: dell-latitude-5400-8665U-sarien-cbg-4
      servod: docker
      console: servoMicro
      serial: CMO653-00166-040491U00139
      port: 60364
      type: sarien
      ctrls: power_state
      cr50_log: true
      ec_log: true
      ykush:
        driver: YKUSHXS
        serial: YKD5454
      mac: 06:dd:2f:97:dc:3d
      ip: 192.168.201.11
  others:
    - name: sm8350-hdk-cbg-4
      console: FTDI
      serial: AU06CC7Z
      type: sm8350-hdk
      ykush:
        driver: YKUSH3
        serial: Y3N10089
      mac: 8c:fd:f0:49:29:74
      ip: 192.168.201.13
pdus:
  - name: eth484-1
    driver: devantech_eth484
    mac: e8:eb:1b:d4:42:5b
    ip 192.168.201.9
EOF
```

With this in place you can run the ansible to update your dispatcher. 
This will need to be run from the `ansible` folder:

> **Note** You can test your ansible setup without making changes on the 
> target and displaying the changes with `--check --diff` flags.

```
./hosts/lava-rack-cbg-x/deploy/all

```

