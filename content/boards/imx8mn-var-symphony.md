---
title: i.MX8MN Variscite Symphony board
---

Variscite [Symphony-Board](https://www.variscite.com/product/single-board-computers/symphony-board/)
Single Board Computer (SBC) in conjunction with [VAR-SOM-MX8M-NANO](https://www.variscite.com/product/system-on-module-som/cortex-a53-krait/var-som-mx8m-nano-nxp-i-mx-8m-nano/)
System on Module (SoM) based on NXP’s [i.MX 8M Nano](https://www.nxp.com/products/processors-and-microcontrollers/arm-processors/i-mx-applications-processors/i-mx-8-processors/i-mx-8m-nano-family-arm-cortex-a53-cortex-m7:i.MX8MNANO).

Variscite provides two [evaluation kits](https://www.variscite.com/product/evaluation-kits/var-som-mx8m-nano-evaluation-kits/)
variants: Starter Kit and Development Kit, which comes with Display and Touch
Panel.

![iMX8MN Variscite Development Kit](/img/imx8mn_symphony_kit.jpg)

Specifications [VAR-SOM-MX8M-NANO](http://variwiki.com/index.php?title=VAR-SOM-MX8M-NANO):
* NXP i.MX 8M Nano:
  * 4x ARM Cortex-A53 @ 1.5GHz
  * 1x Cortex-M7 @ 750MHz
* Memory: 1GB DDR4-2400
* GPU: Vivante GC7000UL 3D
* Display:
  * MIPI DSI 1080p60
  * Dual LVDS 1080p60
* Storage:
  * eMMC up to 64GB,
* SD card
* Network:
  * Ethernet: 10/100/1000 Mbps,
  * WiFi: Certified 802.11 a/b/g/n/ac,
  * Bluetooth: 4.2, BLE
* RTC on carrier
* 3x I2C
* 3x SPI
* 4x UART
* 1x USB 2.0 OTG
* Audio:
  * Headphone
  * Microphone: Digital, Analog (stereo)
  * 5x I2S(SAI), S/PDIF, PDM, Line In/Out
* Temperature Range: -40 to 85°C
* Dimensions: 67.8mm x 33.0mm

### Power control

Board can be powered via a DC Power Jack (J24) 12V/3A DC input, center positive.
A Power ON Switch (SW7) is placed right next to the DC Jack.

### Boot selection

![iMX8MN Variscite Boot Switch](/img/imx8mn_symphony_boot.jpg)

While the board is powered OFF, the Boot Select Switch (SW3) can be set to
choose the booting device: INT (eMMC/NAND) or SD (micro SD card).

### Low-level boot control

Serial connection to the board can be done plugging a USB type A to micro B
cable to the USB Debug Connector (J29). Serial port settings 115200 8N1.

#### Network connectivity

There is a standard Gigabit Ethernet Jack (J21) on the device that can be used.

### Bootloader

For the bootloader the Apertis u-boot build should be used (as it allows for
testing Apertis specifics).

The main bootloader is stored in eMMC. Documentation on how to install this can
be found on the [apertis setup page](https://www.apertis.org/reference_hardware/imx8mn_var_symphony_setup/)

Load addresses:
* kernel: 0x40480000
* dtb: 0x43000000
* ramdisk: 0x43800000

### Health checks

* Mainline Linux kernel support in ARM64 defconfig since release v5.10.
* Apertis Linux ARM64 package support since release 5.10.40-1+apertis1.

### Lab notes and trouble shooting

None.
