---
title: Corsola Chromebooks
---

`corsola` is a board name for arm64 Chromebooks based on the Mediatek MT8186 SoC.

The Collabora LAVA lab contains the following `corsola` devices:

-   [Lenovo 300e Yoga Chromebook Gen 4](https://www.lenovo.com/gb/en/p/laptops/lenovo/lenovo-edu-chromebooks/lenovo-300e-yoga-chromebook-gen-4/len101l0029) (codename `steelix`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, STEELIX, DVT1, GB, SKU6, MEDIATEK 8186, 8GB RAM, 64GB, 11.6IN, HD, WIFI, BT`
    -   See [mt8186-corsola-steelix-sku131072](https://lava.collabora.dev/scheduler/device_type/mt8186-corsola-steelix-sku131072) in LAVA
    -   CPU: 2x Arm Cortex-A76 cores up to 2GHz, 6x Arm Cortex-A55 cores up to 2GHz
    -   GPU: Arm Mali G52 MC2 2EE
    -   SoC: Mediatek Kompanio 520 (MT8186)
    -   Arch: Aarch64
    -   AUE Date: 2033-06

Full SoC specs: <https://www.mediatek.com/products/chromebooks/mediatek-kompanio-520>

Mainline Linux kernel support not upstream at the date of writing.

SoC support added in release v6.1-rc1: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/arch/arm64/boot/dts/mediatek/mt8186.dtsi?h=v6.1-rc1&id=2e78620b13509787a239c847b1ba576265a38bc4>

### Debugging interfaces

`corsola` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In the Lenovo 300e Yoga Chromebook Gen 4, the debug port is the USB-C port on the left side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through the Asix AX88772B AX88772 USB-Eth adapter.

WiFi connectivity on this device is provided by the MediaTek MT7921s chip; the required firmware binaries can be found in the [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/) repository:
- mediatek/WIFI_MT7961_patch_mcu_1_2_hdr.bin
- mediatek/WIFI_RAM_CODE_MT7961_1.bin

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `steelix` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/corsola.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.
