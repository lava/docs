---
title: tags
---

### Overview
Tags can be used within LAVA to specify certain aspects of a device that may 
differ from what you would expect like if there is a certain piece of hardware 
available on only on or two devices of a type. There are details and examples 
of use in the [LAVA Docs](https://lava.collabora.dev/static/docs/v2/developing-tests.html?highlight=tags#using-device-tags).

### Collabora Lab
In the Collabora Lava Lab there are a few tags we use for specific things, the most noticable are `chromeos` `cbg-X` and `battery`. This is how they would look in the web interface:

![dedede devices tag list](/img/dedede_tags.png)

### Chromeos
The `chromeos` tag is used to identify the devices that are part of the Chromeos Lab. This tag is also used for the purposes of pulling together reports.

### cbg-X
The cbg-X tags are used to identify devices by their name. Typically the cbg-0 device will be the one that was set up on staging for the initial bringup, but if you wanted to target the same device for a series of tests, you could use the cbg-X tag with the number of the device to ensure the same device physical device is used for all of your tests.

### Battery
In the Collabora Lab we have some devices that have been working for a few years now and are getting tired. After some investigation it was found that having the battery connected caused some failures. These devices have had the batteries removed so that we can continue to run tests on them in the lab. As you can see from the above image, acer-cb317-1h-c3z6-dedede-cbg-6 has no battery connected. If you were wanting to test features requiring the battery you'd need to add a battery tag as shown below.

### Usage
Tags wihtin lava need to be added as a top level list, along with `device_type` and `timeouts`. So to ensure your test runs on a device with a battery your test would start something like:

```
# lava job definition for a dedede device that has a battery...
device_type: acer-cb317-1h-c3z6-dedede
job_name: Dedede battery test

tags:
  - battery

timeouts:
  job:
...
```

