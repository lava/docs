---
title: Skyrim Chromebooks
---

`skyrim` is a board name for x86_64-based Chromebooks.

The Collabora LAVA lab contains the following `skyrim` devices:

-   [Dell Latitude 3445 Chromebook](https://www.dell.com/en-us/shop/dell-laptops/latitude-3445-chromebook/spd/latitude-14-3445-chrome-laptop/s004lc3445usvp) (codename `whiterun`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, WHITERUN, DVT1,US, SKU10, RYZEN 5 7520C, 8GB RAM, 256GB, 14IN, FHD, WIFI, BT `
    -   See [dell-latitude-3445-7520c-skyrim](https://lava.collabora.dev/scheduler/device_type/dell-latitude-3445-7520c-skyrim) in LAVA
    -   CPU: AMD Ryzen 5 7520C with Radeon Graphic
    -   GPU: AMD Radeon 1506
    -   Arch: x86_64
    -   AUE Date: 2033-06

### Debugging interfaces

`skyrim` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables.

In an Dell Latitude 3445 Chromebook, the debug port is the USB-C port on the right side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

WiFi connectivity on this device is provided by the MediaTek MT7921e chip; the required firmware binaries can be found in the [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/) repository:
- mediatek/WIFI_MT7961_patch_mcu_1_2_hdr.bin
- mediatek/WIFI_RAM_CODE_MT7961_1.bin

#### Known issues

See [Common issues](../common_issues/).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `winterhold` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/skyrim.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `dell-latitude-3445-7520c-skyrim`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path            Device  Class          Description
    ======================================================
    /0/0                        memory         1MiB BIOS
    /0/4                        processor      AMD Ryzen 5 7520C with Radeon Graphic
    /0/4/6                      memory         128KiB L1 cache
    /0/4/7                      memory         2MiB L2 cache
    /0/4/8                      memory         4MiB L3 cache
    /0/5                        memory         128KiB L1 cache
    /0/a                        memory         8GiB System Memory
    /0/a/0                      memory         4GiB Row of chips Synchronous 6400 MH
    /0/a/1                      memory         4GiB Row of chips Synchronous 6400 MH
    /0/100                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/100/2.1                  bridge         Advanced Micro Devices, Inc. [AMD]
    /0/100/2.1/0                network        MEDIATEK Corp.
    /0/100/2.3                  bridge         Advanced Micro Devices, Inc. [AMD]
    /0/100/8.1                  bridge         Advanced Micro Devices, Inc. [AMD]
    /0/100/8.1/0                display        Advanced Micro Devices, Inc. [AMD/ATI
    /0/100/8.3                  bridge         Advanced Micro Devices, Inc. [AMD]
    /0/100/14.3                 bridge         FCH LPC Bridge
    /0/101                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/102                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/103                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/104                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/105                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/106                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/107                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/108                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/109                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/10a                      bridge         Advanced Micro Devices, Inc. [AMD]
    /0/10b                      bridge         Advanced Micro Devices, Inc. [AMD]
    /1                  eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 14b5 (rev 02)
    00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Device 14b6
    00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 14b7 (rev 02)
    00:02.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 14b7 (rev 02)
    00:02.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Device 14ba (rev 02)
    00:02.3 PCI bridge: Advanced Micro Devices, Inc. [AMD] Device 14ba (rev 02)
    00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 14b7 (rev 02)
    00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Device 14b9
    00:08.3 PCI bridge: Advanced Micro Devices, Inc. [AMD] Device 14b9
    00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 71)
    00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
    00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1724
    00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1725
    00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1726
    00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1727
    00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1728
    00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 1729
    00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 172a
    00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Device 172b
    01:00.0 Network controller: MEDIATEK Corp. Device 7961
    02:00.0 Non-Volatile memory controller: Samsung Electronics Co Ltd Device a80b (rev 02)
    04:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Device 1506 (rev d8)
    04:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Device 1640
    04:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] VanGogh PSP/CCP
    04:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Device 1503
    04:00.4 USB controller: Advanced Micro Devices, Inc. [AMD] Device 1504
    04:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor (rev 6f)
    04:00.7 Signal processing controller: Advanced Micro Devices, Inc. [AMD] Device 164a
    05:00.0 USB controller: Advanced Micro Devices, Inc. [AMD] Device 1505
    ```
