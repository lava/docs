---
title: ROCK 4
---

ROCK 4 is a family of Rockchip RK3399 based SBC (Single Board Computer)
by Radxa.

There are multiple variants of the board: A, B, C, SE, C+. The main
difference between the boards being different WiFi and Bluetooth chips and
the presence of DP and HDMI mini on C and C+. More information and schematics
are available from [Radxa Documentation](https://wiki.radxa.com/Rockpi4/getting_started).

Specification:
* Rockchip Rk3399 SoC
* 2x ARM Cortex-A72 @ 1.8GHz, 4x ARM Cortex-A53 @ 1.4GHz
* 1/2/4GB memory
* Mali T860MP4 GPU
* MIPI CSI 2 lanes connector
* eMMC module connector
* uSD slot (up to 128GB)
* 4x USB
* 1x HDMI
* Ethernet port
* 40-pin IO header including UART, SPI, I2C and 5V DC power in
* USB PD over USB Type-C
* Size: 85mm x 54mm

This board is available from [Radxa distributors](https://wiki.radxa.com/Buy).

### Power control

The board can be powered on with 5V via pins 2 or 4 in the GPIO header (as shown
in the [GPIO pinout][1]) and GND on pin 6 for example. The pins are colored
accordingly on the latest board revision:

![Power through GPIO](/img/rockpi4-power-gpio.jpg)

Alternatively, the board can be powered via a USB PD Type-C connector:
* 9V/2A, 12V/2A, 15V/2A, 20V/2A

### Low-level boot control

Serial connection on the board can be done through pins on the GPIO. The board's
TX is on pin 8 and RX on pin 10 as per [the pinout][1]. Serial specification:
* 3.3V TTL
* 1500000 8n1

![Serial through GPIO](/img/rockpi4-serial-gpio.jpg)

#### Network connectivity

There's a standard Gigabit Ethernet port on the board that can be used.

### Bootloader

The boot order for RK3399 is generally:
1) SPI Flash (not populated on all ROCK 4 variants)
1) eMMC
1) SD card
1) maskrom (Rockchip-specific USB download mode)

If any device is not found or cannot be booted from, the next one is the
list is chosen until the device falls into Maskrom mode (Rockchip-specific
USB flashing mode).


### Building U-Boot

Mainline U-Boot version v2024.07-rc4 supports booting ROCK 4 from SPI flash.

```
sudo apt install git crossbuild-essential-arm64

cd rkbin
./tools/boot_merger RKBOOT/RK3399MINIALL.ini
./tools/boot_merger RKBOOT/RK3399MINIALL_SPINOR.ini

cd trusted-firmware-a
git checkout v2.9.0
make CROSS_COMPILE=aarch64-linux-gnu- PLAT=rk3399 -j$(nproc) bl31

cd u-boot
git checkout v2024.07-rc4
make CROSS_COMPILE=aarch64-linux-gnu- BL31=../trusted-firmware-a/build/rk3399/release/bl31/bl31.elf rock-pi-4-rk3399_defconfig all -j$(nproc)
```

### Flashing U-Boot

There are three choices of where to install the bootloader, listed here in order
of boot priority. The bootloader only needs to be installed to one device. The
SPI flash is preferred. The boot devices are as follows:

![Boot devices](/img/rockpi4-flash.jpg)

Maskrom flashing is required for SPI Flash and eMMC booting. For SD card booting,
ensure there is no SPI flash and eMMC connected (or they are blank) and skip to the
`Flashing bootloader to SD Card` section.

Install the required packages to flash the device:
```
sudo apt install rkdeveloptool
```

To force the device into Maskrom flashing, before powering the device:
1) remove the SD card
1) remove the eMMC
1) add a jumper to disable the SPI Flash (see below)

To disable booting from the SPI flash, place a 0.1" pitch jumper between GPIO
header pins 23 and 25 as follows:

![Disable SPI flash](/img/rockpi4-disable-spi.jpg)

You need a USB A-A cable to flash the bootloader to the SPI flash or eMMC using
the rockusb protocol. The rockusb flashing port is the top USB3 port (but flashing
occurs over USB2). Power is not provided from this port, so make sure to power
over USB-C using a dumb PSU (e.g Raspberry Pi 4 PSU).


Plug a USB-2 A-A cable into the top USB-3 socket.

Apply power through the USB-C socket and check it is in maskrom mode:

```
rkdeveloptool ld
DevNo=1	Vid=0x2207,Pid=0x330c,LocationID=602	Maskrom
```


### Flashing bootloader to SPI Flash

Not all devices have an SPI flash. If it is not present on the board, it needs
to be soldered to the board OR boot from alternative media.

Force the device into Maskrom flashing following the steps in `Flashing U-Boot`.

Unplug the SPI flash jumper.

Download the SPI NOR loader:
```
$ rkdeveloptool db rk3399_loader_spinor_v1.30.114.bin
Downloading bootloader succeeded.
```

Flash the bootloader to the SPI flash:
```
$ rkdeveloptool wl 0 u-boot-rockchip-spi.bin
```

All done.


#### Flashing bootloader to eMMC

Force the device into Maskrom flashing following the steps in `Flashing U-Boot`.

Leave the SPI flash jumper connected, if there is an SPI flash placed on the board.

Plug the eMMC into the board.

Download the eMMC loader:
```
$ rkdeveloptool db rk3399_loader_v1.30.114.bin
Downloading bootloader succeeded.
```

Flash the bootloader to the eMMC:
```
$ rkdeveloptool wl 64 u-boot-rockchip.bin
```

All done.


#### Flashing bootloader to SD Card

```
$ dd if=u-boot-rockchip.bin of=/dev/sdb seek=64
```


### Lab notes and trouble shooting

None.

[1]: https://wiki.radxa.com/Rockpi4/hardware/gpio
