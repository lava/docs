
---
title: Mediatek (MT8390) Genio 700
---

The MediaTek Genio 700 (MT8390) is a high-performance edge-AI IoT platform 
designed for smart home, interactive retail, industrial and commercial applications. 
[Taken from MediaTek Documentation](https://mediatek.gitlab.io/aiot/doc/aiot-dev-guide/master/qsg/qsg_genio_700_intro.html)

Specification:
* MediaTek CPU (MT8390)
* MediaTek PMIC (MT6365)
* 4GB + 4GB LPDDR4X RAM (Micron MT53E1G32D2FW-046 IT: B)
* 64GB eMMC5.1 x 1 (WD SDINBDG4-64G-XI2)
* 2.0mm DC Jack x 1 (for 12V DC input)
* Micro SD card slot x 1
* Push button x 4 (power, reset, download, and home key)
* LED x 4 (power, reset, system on, and charging status)
* 4-lane DSI x 2
* eDP x 1
* HDMI 2.0 x 1
* 10/100/1000M Ethernet x 1 (shared with DPI signal)
* USB device port x 1 (Micro USB connector)
* USB host port x 1 (USB Type-C™ connector)
* 3.5mm earphone jack x 1 (with microphone input)
* 3.5mm line out audio jack x 1
* Analog microphone x 1
* Digital microphone x 2
* UART port x 3 for the trace log with USB to UART bridge IC (Micro USB connector x 3)
* I2C capacitive touchpad
* 4-data lane CSI x 2
* M.2 slot x 2 (for AzureWave AW-XB468NF Wi-Fi module、AW-CB451NF Wi-Fi module)
* 40-pin 2.54mm pin header x 1 (for Raspberry Pi like I/O interface)

This board is available from [Mouser Electronics](https://www.mouser.co.uk/).

### Power control

The board is be powered with a 12v 4.16A Power supply via 5.5x2.1mm barrel connector 
that is supplied with the board. 

We are able to control power directly by connecting the power supply through and ethernet 
relay for power cycling. We are also able to control resets with the inboard GPIO through 
UART0 used for the console access.

#### GPIO setup

GPIO access can be set up using genio-tools.

Installing the tools on a debian system is best done using a [virtual environment](https://docs.python.org/3/library/venv.html)
You will need to have libusb-1.0-0 installed on your system as well.

    sudo apt install python3-virtualenv libusb-1.0-0

And create your virtual enviroment, install the tools and update the board for GPIO access:

    mkdir genio-tools
    virtualenv genio-tools
    source ./genio-tools/bin/activate
    pip3 install -U genio-tools

Once the installer is complete you can check that the board is available with:

    genio-board list

And you should get one available insterface, if you have more than one the programming will fail:

    Available insterfaces:
      ftdi://ftdi:232:B0036PX0/1  (FT232R USB UART)

You can then program the board:

    genio-board program-ftdi --ftdi-product-name i700-evk --gpio-power 0 --gpio-reset 1 --gpio-download 2

You should then see a success message:

    INFO:aiot:FTDI device programmed successfully

And you can re-plug the usb connection to UART0 and then test the update by running the power command:

    genio-board power

You should then see the red 'System on LED' light up. With the stock android, this will go off again after 
a second but the power control is working through the GPIO.

Power and uart0 connected to the board:

![Power and uart0 connected](/img/genio_700_power_uart0.jpg)


### Console connectivity

Serial connection on the board can be done through the USB micro connection labelled UART0 with a 
USB type-A to USB micro cable. The connection details for the serial console are:

* 921600 8N1


#### Network connectivity

To Be Completed

### Bootloader

To Be Completed

### Lab notes and trouble shooting

None.
