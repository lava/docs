---
title: uhubctl
---

### Initial testing and usage 

uhubctl has been identified as a way to manage the USB connections of the Devices to the USB hub, written in python. While this can be used to power off the USB connection to the device from the dispatcher (controller) there are other moving parts to be considered before this can be used in production, especially for the chromebooks. Details can be found at https://github.com/mvp/uhubctl.

### Listing devices

By running `uhubctl` without any arguments you wil be able to see the devices connected to the hub.

```
root@lava-rack-cbg-3:~# uhubctl 
Current status for hub 1-3.2 [05e3:0610 USB2.0 Hub, USB 2.00, 4 ports]
  Port 1: 0100 power
  Port 2: 0100 power
  Port 3: 0100 power
  Port 4: 0100 power
Current status for hub 1-3 [05e3:0610 USB2.0 Hub, USB 2.00, 4 ports]
  Port 1: 0503 power highspeed enable connect [0557:7000, USB 2.00, 4 ports]
  Port 2: 0507 power highspeed suspend enable connect [05e3:0610 USB2.0 Hub, USB 2.00, 4 ports]
  Port 3: 0100 power
  Port 4: 0100 power
Current status for hub 4-4.4 [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 1: 02a0 power 5gbps Rx.Detect
  Port 2: 02a0 power 5gbps Rx.Detect
  Port 3: 02a0 power 5gbps Rx.Detect
  Port 4: 02a0 power 5gbps Rx.Detect
Current status for hub 4-4.3 [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 1: 02a0 power 5gbps Rx.Detect
  Port 2: 02a0 power 5gbps Rx.Detect
  Port 3: 02a0 power 5gbps Rx.Detect
  Port 4: 02a0 power 5gbps Rx.Detect
Current status for hub 4-4.2 [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 1: 02a0 power 5gbps Rx.Detect
  Port 2: 02a0 power 5gbps Rx.Detect
  Port 3: 02a0 power 5gbps Rx.Detect
  Port 4: 02a0 power 5gbps Rx.Detect
Current status for hub 4-4.1 [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 1: 02a0 power 5gbps Rx.Detect
  Port 2: 02a0 power 5gbps Rx.Detect
  Port 3: 02a0 power 5gbps Rx.Detect
  Port 4: 02a0 power 5gbps Rx.Detect
Current status for hub 4-4 [05e3:0617 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 1: 0263 power 5gbps U3 enable connect [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 2: 0263 power 5gbps U3 enable connect [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 3: 0263 power 5gbps U3 enable connect [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
  Port 4: 0263 power 5gbps U3 enable connect [05e3:0612 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
Current status for hub 4 [1d6b:0003 Linux 5.10.0-3-amd64 xhci-hcd xHCI Host Controller 0000:01:00.0, USB 3.00, 4 ports]
  Port 1: 02a0 power 5gbps Rx.Detect
  Port 2: 02a0 power 5gbps Rx.Detect
  Port 3: 02a0 power 5gbps Rx.Detect
  Port 4: 0263 power 5gbps U3 enable connect [05e3:0617 GenesysLogic USB3.0 Hub, USB 3.00, 4 ports]
Current status for hub 3-1.4.4 [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 1: 0100 power
  Port 2: 0100 power
  Port 3: 0100 power
  Port 4: 0100 power
Current status for hub 3-1.4.3 [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 1: 0100 power
  Port 2: 0100 power
  Port 3: 0100 power
  Port 4: 0100 power
Current status for hub 3-1.4.2 [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 1: 0503 power highspeed enable connect [18d1:501f Google Inc. SuzyQable 001A00280060, USB 2.00, 2 ports]
  Port 2: 0503 power highspeed enable connect [18d1:501f Google Inc. SuzyQable 0026002203BB, USB 2.00, 2 ports]
  Port 3: 0503 power highspeed enable connect [18d1:501f Google Inc. SuzyQable 00190027032F, USB 2.00, 2 ports]
  Port 4: 0100 power
Current status for hub 3-1.4.1 [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 1: 0503 power highspeed enable connect [18d1:501f Google Inc. SuzyQable 0005000D015E, USB 2.00, 2 ports]
  Port 2: 0503 power highspeed enable connect [18d1:501f Google Inc. SuzyQable 0015000B00DF, USB 2.00, 2 ports]
  Port 3: 0503 power highspeed enable connect [18d1:501f Google Inc. SuzyQable 0020002201E8, USB 2.00, 4 ports]
  Port 4: 0503 power highspeed enable connect [18d1:501f Google Inc. SuzyQable 0010000C013C, USB 2.00, 2 ports]
Current status for hub 3-1.4 [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 1: 0503 power highspeed enable connect [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 2: 0503 power highspeed enable connect [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 3: 0507 power highspeed suspend enable connect [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
  Port 4: 0507 power highspeed suspend enable connect [05e3:0610 GenesysLogic USB2.0 Hub, USB 2.10, 4 ports]
Current status for hub 3 [1d6b:0002 Linux 5.10.0-3-amd64 xhci-hcd xHCI Host Controller 0000:01:00.0, USB 2.00, 1 ports]
  Port 1: 0503 power highspeed enable connect [2109:3431 USB2.0 Hub, USB 2.10, 4 ports]
```

### turning power off and on to ports

From here you can see the details of a usb device connected to the hub, the port number on the hub, and the details of the hub the device is connected to.

To turn off the power for the usb port for the device '[18d1:501f Google Inc. SuzyQuable 00190027032F,...]' you can use the command:

```
uhubctl -a off -l 3-1.4.2 -p 3
``` 

Where the -a flag indicates the action, one of `off` `on` `cycle` `toggle` (or `0`/`1`/`2`/`3`).
Where the -l flag indicates the location, identifiesd by the 'Current status' message.
Where the -p flag indicated the port, identified by the port numnber for the device in question.

With the -a flag, the cycle or 2 action can take an extra parameter of -d with the number of seconds to delay from the off action before the on action.
