---
title: Brask Chromeboxes
---

`brask` is a board name for x86_64-based Chromeboxes.

The Collabora LAVA lab contains the following `brask` devices:

-   [Acer Chromebox CXI5](https://www.acer.com/us-en/desktops-and-all-in-ones/acer-chromebox/acer-chromebox-cxi5/pdp/DT.Z2JAA.001) (codename `moli`)
    -   Label: `TLA, DESKTOP, CHROMEBOX, MOLI, PVT1, SKU 1-2, INTEL CELERON 7305U, 4GB RAM, 32GB, WIFI, BT`
    -   See [acer-chromebox-cxi5-brask](https://lava.collabora.dev/scheduler/device_type/acer-chromebox-cxi5-brask) in LAVA
    -   CPU: Intel Celeron CPU 7305
    -   GPU: Intel UHD Graphics
    -   Arch: x86_64
    -   AUE Date: 2032-06

### Debugging interfaces

`brask` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cables for the initial bring-up work. As Chromeboxes don't have a battery, ServoMicro is recommended once the device is installed in the lab. The 'brask' devices in the lab do not have a servov4 header so they are reliably running over SuzyQ.

In an Acer Chromebox CXI5, the debug port is the USB-C port next to the charger port.

#### Network connectivity

In the lab, these Chromeboxes are connected to the network through a
Techrise USB-Eth adapter (R8152-based, to provide connectivity at bootloader level) and through the onboard ethernet port (controller is a Realtek R8169).

WiFi connectivity on this device is provided by the Intel Wi-Fi 6E AX211 chip; the required firmware binaries can be found in the [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/) repository:
- iwlwifi-so-a0-gf-a0-86.ucode
- iwlwifi-so-a0-gf-a0.pnvm

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `kaisa` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/brask.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `acer-chromebox-cxi5-brask`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      Intel(R) Celeron(R) 7305
    /0/4/6                 memory         64KiB L1 cache
    /0/4/8                 memory         8MiB L3 cache
    /0/5                   memory         96KiB L1 cache
    /0/b                   memory         4GiB System Memory
    /0/b/0                 memory         4GiB SODIMM DDR4 Synchronous 3200 MHz (0.3
    /0/100                 bridge         Intel Corporation
    /0/100/2               display        Intel Corporation
    /0/100/7               bridge         Intel Corporation
    /0/100/7.1             bridge         Intel Corporation
    /0/100/7.2             bridge         Intel Corporation
    /0/100/14.2            memory         RAM memory
    /0/100/14.3            network        Intel Corporation
    /0/100/1c              bridge         Intel Corporation
    /0/100/1c/0    eth0    network        RTL8111/8168/8411 PCI Express Gigabit Ethe
    /0/100/1c.7            bridge         Intel Corporation
    /0/100/1d              bridge         Intel Corporation
    /0/100/1f              bridge         Intel Corporation
    /1             eth1    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Device 4619 (rev 04)
    00:02.0 VGA compatible controller: Intel Corporation Device 46b3 (rev 0c)
    00:04.0 Signal processing controller: Intel Corporation Device 461d (rev 04)
    00:07.0 PCI bridge: Intel Corporation Device 466e (rev 04)
    00:07.1 PCI bridge: Intel Corporation Device 463f (rev 04)
    00:07.2 PCI bridge: Intel Corporation Device 462f (rev 04)
    00:08.0 System peripheral: Intel Corporation Device 464f (rev 04)
    00:0a.0 Signal processing controller: Intel Corporation Device 467d (rev 01)
    00:0d.0 USB controller: Intel Corporation Device 461e (rev 04)
    00:0d.2 USB controller: Intel Corporation Device 463e (rev 04)
    00:0d.3 USB controller: Intel Corporation Device 466d (rev 04)
    00:14.0 USB controller: Intel Corporation Device 51ed (rev 01)
    00:14.2 RAM memory: Intel Corporation Device 51ef (rev 01)
    00:14.3 Network controller: Intel Corporation Device 51f0 (rev 01)
    00:15.0 Serial bus controller [0c80]: Intel Corporation Device 51e8 (rev 01)
    00:15.1 Serial bus controller [0c80]: Intel Corporation Device 51e9 (rev 01)
    00:16.0 Communication controller: Intel Corporation Device 51e0 (rev 01)
    00:1c.0 PCI bridge: Intel Corporation Device 51be (rev 01)
    00:1c.7 PCI bridge: Intel Corporation Device 51bf (rev 01)
    00:1d.0 PCI bridge: Intel Corporation Device 51b3 (rev 01)
    00:1e.0 Communication controller: Intel Corporation Device 51a8 (rev 01)
    00:1e.3 Serial bus controller [0c80]: Intel Corporation Device 51ab (rev 01)
    00:1f.0 ISA bridge: Intel Corporation Device 5182 (rev 01)
    00:1f.3 Multimedia audio controller: Intel Corporation Device 51c8 (rev 01)
    00:1f.4 SMBus: Intel Corporation Device 51a3 (rev 01)
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Device 51a4 (rev 01)
    82:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 1b)
    83:00.0 SD Host controller: Genesys Logic, Inc Device 9755 (rev 01)
    84:00.0 Non-Volatile memory controller: O2 Micro, Inc. Device 8760 (rev 01)
    ```
