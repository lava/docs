---
title: Nissa Chromebooks
---

`nissa` is a board name for x86_64-based Chromebooks.

The Collabora LAVA lab contains the following `nissa` devices:

-   [Acer Chromebook Spin 512 R856LTN](https://www.acer.com/us-en/chromebooks/acer-chromebook-spin-512-r856t-r856tn/pdp/NX.KE4AA.002) (codename `craask`)
    -   Label: `TLA, NOTEBOOK, CHROMEBOOK, CRAASK, DVT, SKU4, INTEL,PENTIUM N200, 8GB RAM 128GB, 12IN, HD, WIFI, BT, LTE`
    -   See [acer-n20q11-r856ltn-p1s2-nissa](https://lava.collabora.dev/scheduler/device_type/acer-n20q11-r856ltn-p1s2-nissa) in LAVA
    -   CPU: Intel N200
    -   GPU: Intel UHD Graphics
    -   Arch: x86_64
    -   AUE Date: 2033-06

### Debugging interfaces

`nissa` boards have been flashed and tested with [SuzyQ](../../01-debugging_interfaces) cable.

In an Acer Chromebook Spin 512, the debug port is the USB-C port on the left side.

#### Network connectivity

In the lab, these Chromebooks are connected to the network through a
Techrise USB-Eth adapter (R8152-based).

WiFi connectivity on this device is provided by the Intel Wi-Fi 6E AX211 chip; the required firmware binaries can be found in the [linux-firmware](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/) repository:
- iwlwifi-so-a0-gf-a0-86.ucode
- iwlwifi-so-a0-gf-a0.pnvm

#### Known issues

See [Common issues](../common_issues.md).

### Example kernel command line arguments

    console=ttyS0,115200n8 root=/dev/nfs rw nfsroot=10.154.97.199:/srv/nfs/chromebook,tcp,hard ip=dhcp tftpserverip=10.154.97.199

The IP and path of the NFS share are examples and should be adapted to
the test setup.

### Firmware

Firmware flashed on all units in the lab (based on the ChromeOS `nissa` image variant) : <https://gitlab.collabora.com/chromium/firmware-tools/-/blob/master/cros-build/firmware/nissa.bin>

See the [firmware-tools documentation](https://gitlab.collabora.com/chromium/firmware-tools) for more info on how the firmware images for the Chromebooks in the lab are built.

### Hardware info

Output of `lshw` and `lspci` commands for the specific device types in the Collabora LAVA lab:

-   `acer-n20q11-r856ltn-p1s2-nissa`:

    ```console
    lshw -c cpu -c display -c network -c bridge -c memory -short
    H/W path       Device  Class          Description
    =================================================
    /0/0                   memory         1MiB BIOS
    /0/4                   processor      Intel(R) N200
    /0/4/6                 memory         128KiB L1 cache
    /0/4/8                 memory         6MiB L3 cache
    /0/5                   memory         64KiB L1 cache
    /0/b                   memory         8GiB System Memory
    /0/b/0                 memory         2GiB Row of chips Synchronous 4800 MHz (0.
    /0/b/1                 memory         2GiB Row of chips Synchronous 4800 MHz (0.
    /0/b/2                 memory         2GiB Row of chips Synchronous 4800 MHz (0.
    /0/b/3                 memory         2GiB Row of chips Synchronous 4800 MHz (0.
    /0/100                 bridge         Intel Corporation
    /0/100/2               display        Intel Corporation
    /0/100/14.2            memory         RAM memory
    /0/100/14.3            network        Intel Corporation
    /0/100/1c              bridge         Intel Corporation
    /0/100/1f              bridge         Intel Corporation
    /1             eth0    network        Ethernet interface
    ```

    ```console
    lspci
    00:00.0 Host bridge: Intel Corporation Device 461b
    00:02.0 VGA compatible controller: Intel Corporation Device 46d0
    00:04.0 Signal processing controller: Intel Corporation Device 461d
    00:05.0 Multimedia controller: Intel Corporation Device 462e
    00:0a.0 Signal processing controller: Intel Corporation Device 467d (rev 01)
    00:0d.0 USB controller: Intel Corporation Device 464e
    00:14.0 USB controller: Intel Corporation Device 54ed
    00:14.2 RAM memory: Intel Corporation Device 54ef
    00:14.3 Network controller: Intel Corporation Device 54f0
    00:15.0 Serial bus controller [0c80]: Intel Corporation Device 54e8
    00:15.1 Serial bus controller [0c80]: Intel Corporation Device 54e9
    00:15.2 Serial bus controller [0c80]: Intel Corporation Device 54ea
    00:15.3 Serial bus controller [0c80]: Intel Corporation Device 54eb
    00:19.0 Serial bus controller [0c80]: Intel Corporation Device 54c5
    00:19.1 Serial bus controller [0c80]: Intel Corporation Device 54c6
    00:1a.0 SD Host controller: Intel Corporation Device 54c4
    00:1c.0 PCI bridge: Intel Corporation Device 54be
    00:1e.0 Communication controller: Intel Corporation Device 54a8
    00:1f.0 ISA bridge: Intel Corporation Device 5481
    00:1f.3 Multimedia audio controller: Intel Corporation Device 54c8
    00:1f.5 Serial bus controller [0c80]: Intel Corporation Device 54a4
    01:00.0 SD Host controller: Genesys Logic, Inc GL9750 SD Host Controller (rev 01)
    ```
