---
title: lava-rack-sjic-0
---

# [lava-rack-sjic-0](https://lava.collabora.co.uk/scheduler/worker/lava-rack-sjic-0) Dispatcher and controller

Dispatcher is [Cyberserve Xeon E-RS100-E10](https://www.broadberry.co.uk/xeon-e-rackmount-servers/cyberserve-rs100-e10-pi2) 
and the server specification can be found on one of [the quotations from broadberry.](/files/PDAR52995.pdf) 
The server can be accessed over SSH and has the IP address of 10.108.97.100. 
The switch for the rack is a [Zyxel GS1900-48](https://www.zyxel.com/uk/en/products_services/8-10-16-24-48-port-GbE-Smart-Managed-Switch-GS1900-Series/specification) 
and is on IP address 10.108.97.200. 
The USB hub is a [Swissonic 16 port USB3 Hub](https://www.thomann.de/gb/swissonic_usb_hub_1916.htm). 


# Devices from top to bottom

[sc7180-trogdor-lazor-limozeen-cbg-7](https://lava.collabora.co.uk/scheduler/device/sc7180-trogdor-lazor-limozeen-cbg-7) 
power connected to the [vertical power bar](https://www.racksolutions.co.uk/vertical-rack-mount-power-strips-8.html), 
connection to USB hub using a [SuzyQable](https://www.sparkfun.com/products/retired/14746) and a [Ykush XS switchable USB hub](https://www.yepkit.com/product/300115/YKUSHXS), 
connection to the switch using network cable and [Asix AX88772B USB Ethernet adapter](https://www.amazon.co.uk/Ethernet-Adapter-AX88772B-Converter-Micro-USB-RJ45/dp/B085X8BJMY/). 

[asus-C433TA-AJ0005-rammus-cbg-2](https://lava.collabora.co.uk/scheduler/device/asus-C433TA-AJ0005-rammus-cbg-2) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable and a Ykush XS switchable USB hub,
connection to the switch using ethernet cable and [TechRise USB Ethernet adapter](https://www.amazon.co.uk/TechRise-Network-Adapter-Gigabit-Ethernet/dp/B087PCKGVW/).

Space

Switch Power connected to the [horizontal power bar](https://www.racksolutions.co.uk/catalog/product/view/id/2722/s/8-way-13-amp-horizontal-rack-mount-power-strips/), 
connection to the core switch, 
connection to the dispatcher and then to each of the devices. 
Configuration found [here](https://gitlab.collabora.com/lava/collabora-lava-setup/-/blob/master/doc/rack-assembly.md#managed-ethernet-switch)

Server connected to the horizontal power bar,
USB connection to the USB hub and network connection to the switch. 
Configured using [chef](https://gitlab.collabora.com/lava/collabora-lava-setup)

USB hub connected to the horizontal power bar, 
Main USB3 type-B to the Disptcer Type-A and then to each of the devices in the rack.

[asus-cx9400-volteer-cbg-2](https://lava.collabora.dev/scheduler/device/asus-cx9400-volteer-cbg-2) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[hp-11A-G6-EE-grunt-cbg-7](https://lava.collabora.co.uk/scheduler/device/hp-11A-G6-EE-grunt-cbg-7) 
power connected to the servov4p1 and a Ykush XS switchable USB hub, 
connection to the USB hub using a [servov4p1](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_v4p1.md) 
and a [USB type A to usb Micro cable](https://cpc.farnell.com/pro-signal/psg91471/lead-usb2-0-a-male-micro-b-male/dp/CS30867), 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[hp-x360-12b-ca0010nr-n4020-octopus-cbg-2](https://lava.collabora.co.uk/scheduler/device/hp-x360-12b-ca0010nr-n4020-octopus-cbg-2) 
power connected to the vertical power bar and a Ykush XS switchable USB hub, 
connection to the USB hub using a SuzyQable, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[hp-x360-14-G1-sona-cbg-2](https://lava.collabora.co.uk/scheduler/device/hp-x360-14-G1-sona-cbg-2) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable and a Ykush XS switchable USB hub, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[lenovo-TPad-C13-Yoga-zork-cbg-2](https://lava.collabora.co.uk/scheduler/device/lenovo-TPad-C13-Yoga-zork-cbg-2) 
power connected to the servov4, 
connection to the USB hub using a [servov4](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_v4.md) and a Ykush XS switchable USB hub,
and a [USB type A to usb Micro cable](https://cpc.farnell.com/pro-signal/psg91471/lead-usb2-0-a-male-micro-b-male/dp/CS30867), 
connection to the switch using a network cable from the servov4.

[dell-latitude-5400-8665U-sarien-cbg-2](https://lava.collabora.co.uk/scheduler/device/dell-latitude-5400-8665U-sarien-cbg-2) 
power connected to the vertical power bar, 
connection to the USB hub using [servoMicro](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_micro.md) and a Ykush XS switchable USB hub,
and a [USB type A to usb Micro cable](https://cpc.farnell.com/pro-signal/psg91471/lead-usb2-0-a-male-micro-b-male/dp/CS30867),
connection to the switch using ethernet cable and TechRise USB Ethernet adapter 
and another cable from the onboard ethernet to the switch.

[acer-cb317-1h-c3z6-dedede-cbg-7](https://lava.collabora.co.uk/scheduler/device/acer-cb317-1h-c3z6-dedede-cbg-7) 
Power connected to the vertical power bar,
connection to the USB hub using a SuzyQable and a Ykush XS switchable USB hub, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter, 

[asus-C523NA-A20057-coral-cbg-2](https://lava.collabora.co.uk/scheduler/device/asus-C523NA-A20057-coral-cbg-2) 
power connected to the vertical power bar, 
connection to the USB hub using a SuzyQable and a Ykush XS switchable USB hub, 
connection to the switch using ethernet cable and TechRise USB Ethernet adapter.

[asus-C436FA-Flip-hatch-cbg-2](https://lava.collabora.co.uk/scheduler/device/asus-C436FA-Flip-hatch-cbg-2) 
power connected to the servov4p1, 
connection to the USB hub using a [servov4p1](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/HEAD/docs/servo_v4p1.md) 
and a USB type A to USB Micro cable
connection to the switch using ethernet cable and the servov4p1.

[rk3399-gru-kevin-cbg-2](https://lava.collabora.co.uk/scheduler/device/rk3399-gru-kevian-cbg-2) 
power connected to the vertical power bar, 
connection to the USB hub using servoMicro and a USB type A to usb Micro cable 
and a Ykush XS switchable USB hub, 
connection to the switch using a network cable and a Asix AX88772B USB Ethernet adapter.
